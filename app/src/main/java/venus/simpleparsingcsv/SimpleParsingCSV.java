package venus.simpleparsingcsv;

import android.app.Application;

/**
 * Created by adicatur on 1/2/17.
 */

public class SimpleParsingCSV extends Application {

    //create singleton for make easier any call who call it

    private static SimpleParsingCSV INSTANCE;

    public static Application getInstance(){
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }
}
