package venus.simpleparsingcsv.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import venus.simpleparsingcsv.R;
import venus.simpleparsingcsv.pojo.Example;
import venus.simpleparsingcsv.ui.adapter.FileAdapter;
import venus.simpleparsingcsv.util.FileUtil;

public class ListFileActivity extends AppCompatActivity {
    private static String KEY_FILE = "file";

    private Button button;
    private List<Example> listfiles;
    private ListView listViewFile;


    //create method for passing value from previous activity
    //this case we pass address file e.g /storage/download......, making easier to parsing it
    public static Intent generateIntent(Context context, String file){
        Intent intent = new Intent(context, ListFileActivity.class);
        intent.putExtra(KEY_FILE, file);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_file);

        listViewFile = (ListView) findViewById(R.id.list_files);

        listfiles = new ArrayList<>();

        listViewFile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Example example = listfiles.get(i);
                Toast.makeText(ListFileActivity.this, example.getId() + example.getName() +
                        example.getNumber(), Toast.LENGTH_SHORT).show();
            }
        });
        parseCSV(getIntent().getStringExtra(KEY_FILE));
    }

    //for parsing needs, every csv file will be splitted into coloumn
    private void parseCSV(String files) {

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(files));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] splittedFile = line.split(cvsSplitBy);


                //here Example file, which has only Id, Name , Column
                //you can custom how many coloumn do you need based on your cases

                Example example = new Example();
                example.setId(splittedFile[0]);
                example.setName(splittedFile[1]);
                example.setNumber(splittedFile[2]);

                listfiles.add(example);

                //make sure you splitted well
                System.out.println("example ctr [code= " + splittedFile[0] + " , name=" +
                        splittedFile[1] + "]" + "number " + splittedFile[2]);

            }
            lookUpList();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        listViewFile.setAdapter(new FileAdapter(ListFileActivity.this, listfiles));
    }

    //just testing only, is it realy our file you can delete it when you dont need it
    private void lookUpList(){
        if(!listfiles.isEmpty()){
            for (Example ex : listfiles) {
                Log.d("ctr", "lookUpList: ctr" + ex.getName() + ex);
            }
        }
    }
}
