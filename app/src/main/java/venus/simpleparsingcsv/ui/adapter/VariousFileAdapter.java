package venus.simpleparsingcsv.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import venus.simpleparsingcsv.pojo.File;

/**
 * Created by adicatur on 1/2/17.
 */

public class VariousFileAdapter extends BaseAdapter {

    private Context context;
    private List<File> listFile;
    private LayoutInflater inflater;

    public VariousFileAdapter(Context context, List<File> listFile) {
        this.context = context;
        this.listFile = listFile;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listFile.size();
    }

    @Override
    public File getItem(int position) {
        return listFile.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        Holder holder;

        if(view == null){
            view = inflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
            holder = new Holder();
            holder.textView = (TextView) view.findViewById(android.R.id.text1);

            view.setTag(holder);
        }else {
            holder = (Holder) view.getTag();
        }

        File file = listFile.get(position);

        holder.textView.setText(file.getName());

        return view;
    }

    private static class Holder{
        TextView textView;
    }
}
