package venus.simpleparsingcsv.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import venus.simpleparsingcsv.R;
import venus.simpleparsingcsv.pojo.Example;

/**
 * Created by adicatur on 12/31/16.
 */

public class FileAdapter extends BaseAdapter{
    private List<Example> listFiles;
    private LayoutInflater layoutInflater;

    public FileAdapter(Context context, List<Example> listFiles){
        this.layoutInflater =LayoutInflater.from(context);
        this.listFiles = listFiles;
    }

    @Override
    public int getCount() {
        return listFiles.size();
    }

    @Override
    public Example getItem(int position) {
        return listFiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        Holder holder;
        if(view == null){
            view = layoutInflater.inflate(R.layout.item_list_files, viewGroup, false);
            holder = new Holder();
            holder.textViewId = (TextView) view.findViewById(R.id.text_id);
            holder.textViewName = (TextView) view.findViewById(R.id.text_name);
            holder.textViewNumber = (TextView) view.findViewById(R.id.text_number);
            view.setTag(holder);
        }else{
            holder = (Holder) view.getTag();
        }

        Example example = listFiles.get(position);
        Log.d("ctr", "getView: ctr number " + example.getNumber());
        holder.textViewId.setText(example.getId());
        holder.textViewName.setText(example.getName());
        holder.textViewNumber.setText(example.getNumber());


        return view;
    }

    private static class Holder{
        TextView textViewId;
        TextView textViewName;
        TextView textViewNumber;

    }
}
