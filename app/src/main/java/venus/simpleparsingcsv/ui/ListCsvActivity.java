package venus.simpleparsingcsv.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import venus.simpleparsingcsv.pojo.File;
import java.util.ArrayList;
import java.util.List;

import venus.simpleparsingcsv.R;
import venus.simpleparsingcsv.ui.adapter.VariousFileAdapter;
import venus.simpleparsingcsv.util.Storage;

public class ListCsvActivity extends AppCompatActivity {
    private static int PICK_CSV_REQUEST = 1;

    private Button button;
    private ListView listView;
    private List<File> listFiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_csv);

        button = (Button) findViewById(R.id.btn_upload);
        listView = (ListView) findViewById(R.id.list_files);

        listFiles = new ArrayList<>();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readCsv();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                File file = listFiles.get(position);
                startActivity(ListFileActivity.generateIntent(ListCsvActivity.this, file.getFileAddress()));
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        listFiles = Storage.getInstance().getFiles();
        if(listFiles !=null ){
            listView.setAdapter(new VariousFileAdapter(ListCsvActivity.this, listFiles));

            if(!listFiles.isEmpty()){
                for (File file : listFiles) {
                    Log.d("ctr", "onResume: ctr " + file.getName());
                }
            }
        }
    }

    private void readCsv() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("text/*");
        startActivityForResult(intent, PICK_CSV_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_CSV_REQUEST) {
            try {
                String fileAddress = String.valueOf(data.getData());

                fileAddress = fileAddress.substring(7);
                String fileNameUri = data.getData().getLastPathSegment();

                addFile(fileNameUri, fileAddress);



            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void addFile(String name, String fileAddress){
        File file = new File(name, fileAddress);

//        listFiles.add(file);
        Storage.getInstance().addFile(file);
    }
}
