package venus.simpleparsingcsv.pojo;

/**
 * Created by adicatur on 1/2/17.
 */

public class File {
    private String name;
    private String fileAddress;

    public File(String name, String fileAddress) {
        this.name = name;
        this.fileAddress = fileAddress;
    }

    public String getName() {
        return name;
    }

    public String getFileAddress() {
        return fileAddress;
    }
}
