package venus.simpleparsingcsv.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import venus.simpleparsingcsv.SimpleParsingCSV;
import venus.simpleparsingcsv.pojo.Example;
import venus.simpleparsingcsv.pojo.File;

/**
 * Created by adicatur on 1/1/17.
 */
//create storage for manage cache , for your case it is enough than using SQL database

//using gson for make easier saving object into sharedprefrence

public enum  Storage {
    INSTANCE;

    private String TAG = "Storage";
    private SharedPreferences sharedPreferences;
    private Gson gson;

    Storage(){
        sharedPreferences = SimpleParsingCSV.getInstance().getSharedPreferences(TAG, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    public static Storage getInstance(){
        return INSTANCE;
    }

    public List<Example> getListFile(){
        String json = sharedPreferences.getString("files_csv", "");
        return gson.fromJson(json, new TypeToken<List<String>>(){}.getType());
    }

    //add file for detail file
    public void addFile(Example file){
        List<Example> listFile = getListFile();
        if(listFile == null){
            listFile = new ArrayList<>();
        }

        listFile.add(file);
        String json = gson.toJson(listFile);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        Log.d(TAG, "setListFiles: ctr" + json);

        editor.putString("files_csv", json);
        editor.apply();

    }

    public List<File> getFiles(){
        String json = sharedPreferences.getString("file", "");
        return gson.fromJson(json, new TypeToken<List<File>>(){}.getType());
    }

    //add file for saving file to local when directly you upload file from your local
    public void addFile(File file){
        List<File> files = getFiles();
        if (files == null){
            files = new ArrayList<>();
        }

        files.add(file);
        sharedPreferences.edit()
                .putString("file", gson.toJson(files))
                .apply();
    }
}
